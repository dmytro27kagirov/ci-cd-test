import QtQuick
import QtQuick.Particles

Item {
    property int xPos: 0    // Bubbles position. Default value 0

    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            anchors.fill: parent
            source: Qt.resolvedUrl("assets/bubbles/bubble.png")
        }

        Emitter {
            emitRate: 12
            lifeSpan: config.bubbleLifeSpan
            size: config.bubbleMainSize
            endSize: config.bubbleMainSize*2
            sizeVariation: config.bubbleMainSize/2
            velocity: PointDirection{ y: -200; xVariation: 15; yVariation: 30 }
            x: xPos
            y: window.height
            width: 10
            height: 100
        }
    }

    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            anchors.fill: parent
            source: Qt.resolvedUrl("assets/bubbles/bubble.png")
        }

        Emitter {
            emitRate: 3
            lifeSpan: config.bubbleLifeSpan
            size: config.bubbleMainSize*2
            endSize: config.bubbleMainSize*4
            sizeVariation: config.bubbleMainSize
            velocity: PointDirection{ y: -230; xVariation: 15; yVariation: 30 }
            x: xPos
            y: window.height
            width: 10
            height: 100
        }
    }
}
