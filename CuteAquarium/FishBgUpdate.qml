import QtQuick
import Qt.labs.folderlistmodel
import QtQuick.Controls

Item {
    property int count: 0
    property bool bg: false

    Timer {
        interval: 1000; running: true; repeat: true
        onTriggered: {
            for(; count<listViewFiles.count; count++){
                let name = "fish_" + count + ".png";
                console.log("fish count: " + (count+1))
                console.log("fish added: " + name)
                window.addFish(folderModel.folder + "/" + name)
            }
            if(listViewFiles2.count > 0 && !bg){
                let name = "bg.png";
                console.log("background added: " + name)
                window.setBackground(folderModel2.folder + "/" + name)
                bg = true
            }
        }
    }

    // play once time to set correct dir
    Timer {
        interval: 500; running: true; repeat: false
        onTriggered: {
            folderModel.folder = folderModel.folder + "/assets/fishes"
            folderModel2.folder = folderModel2.folder + "/assets/background"
        }
    }

    ListView {
        id: listViewFiles
        visible: false
        width: 200; height: 400

        FolderListModel {
            id: folderModel
            nameFilters: ["*.png"]
        }

        Component {
            id: fileDelegate
            Text { text: fileName }
        }

        model: folderModel
        delegate: fileDelegate
    }

    ListView {
        id: listViewFiles2
        visible: false
        width: 200; height: 400

        FolderListModel {
            id: folderModel2
            nameFilters: ["*.png"]
        }

        Component {
            id: fileDelegate2
            Text { text: fileName }
        }

        model: folderModel2
        delegate: fileDelegate2
    }
}
