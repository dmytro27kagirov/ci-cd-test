#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <fstream>
#include <iostream>
#include <QTextStream>

#define CONFIG_FILE_PATH "./app.config"

bool loadAppConfiguration();
QTextStream out(stdout);

uint fishWidth;
uint fishHeight;
uint fishMinDuration;
uint fishMaxDuration;
uint fishDeltaWave;
uint bubbleLifeSpan;
uint bubbleMainSize;


int main(int argc, char *argv[])
{
    std::cout << "Start application" << std::endl;
    loadAppConfiguration();

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    const QUrl url("qrc:/CuteAquarium/main.qml");
    QQmlComponent component(&engine, url);

    // Check for errors
    if(component.status() == component.Error)
        out << component.errors()[0].toString() << Qt::endl;

    // Initialize root window
    QObject *window = component.create();

    // Set configuration in qt application
    QMetaObject::invokeMethod(window, "setConfiguration",
              Q_ARG(QVariant, fishWidth),
              Q_ARG(QVariant, fishHeight),
              Q_ARG(QVariant, fishMinDuration),
              Q_ARG(QVariant, fishMaxDuration),
              Q_ARG(QVariant, fishDeltaWave),
              Q_ARG(QVariant, bubbleLifeSpan),
              Q_ARG(QVariant, bubbleMainSize)
    );

    return app.exec();
}

bool loadAppConfiguration(){
    std::ifstream in(CONFIG_FILE_PATH);

    if(!in.is_open()){
        std::cout << "Cannot open app.config file from " << CONFIG_FILE_PATH
                  << ": " << strerror(errno) << std::endl;
        return false;
    }

    std::string param;
    uint value;

    while(!in.eof()){
        in >> param;
        in >> value;
        if(param == "fishWidth")
            fishWidth = value;
        else if(param == "fishHeight")
            fishHeight = value;
        else if(param == "fishMinDuration")
            fishMinDuration = value;
        else if(param == "fishMaxDuration")
            fishMaxDuration = value;
        else if(param == "fishDeltaWave")
            fishDeltaWave = value;
        else if(param == "bubbleLifeSpan")
            bubbleLifeSpan = value;
        else if(param == "bubbleMainSize")
            bubbleMainSize = value;
    }
    in.close();

    // Log the loaded configuration
    std::cout << "App configuration:" << std::endl;
    std::cout << "  fishWidht: " << fishWidth << std::endl;
    std::cout << "  fishHeight: " << fishHeight << std::endl;
    std::cout << "  fishMinDuration: " << fishMinDuration << std::endl;
    std::cout << "  fishMaxDuration: " << fishMaxDuration << std::endl;
    std::cout << "  fishDeltaWave: " << fishDeltaWave << std::endl;
    std::cout << "  bubbleLifeSpan: " << bubbleLifeSpan << std::endl;
    std::cout << "  bubbleMainSize: " << bubbleMainSize << std::endl;

    return true;
}
